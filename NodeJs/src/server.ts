import express, { response } from "express";

import "./database";

const app = express();

/*
    GET; Buscas
    POST: Inserção
    PUT: Alteração
    DELETE: Deletar
    PATCH: Alterar uma informação específica
*/

app.get("/", (request, response) => {
    //return response.send("Hello World")
    return response.json({
        message: "Hello World"
    })
});

app.post("/users", (request, response) =>{
    return response.json({message: "Usuário salvo com sucesso"});
})


const PORTA = 3333;
app.listen(PORTA, ()=> console.log("Server running on port "+PORTA));